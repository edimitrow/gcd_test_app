//
//  AppRouter.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 27/11/2018.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

struct AppScenes {
    
    static let listScene: Int = 0
    static let detailScene: Int = 1
}

class AppRouter: NSObject {
    
    weak var view: ModuleRoutingProtocol?
    weak var rootScene: ModuleRoutingBackProtocol?
    
    init(view: ModuleRoutingProtocol!) {
        self.view = view
    }
    
    init(rootScene:ModuleRoutingBackProtocol) {
        self.rootScene = rootScene
    }
    
    func moveToSceneWithID(_ sceneID: Int) {
        switch sceneID {
        case AppScenes.listScene:
            let dataListViewController: DataListViewController = DataListAssembly.setupScene()
            self.view?.pushToScene(dataListViewController)
            break
        case AppScenes.detailScene:
            
            break
        default:
            break
        }
    }
    
    func moveToRootScene() {
        if let _ = self.rootScene {
            self.rootScene?.calledBackFromRouter()
        }
    }
    
}
