//
//  AssembleHelpers.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 27/11/2018.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

protocol RouteModuleScenes: class {
    func moveToSceneWithID(_ sceneID: Int)
}

protocol ModuleRoutingProtocol: class {
    func pushToScene(_ scene: UIViewController)
    func presentScene(_ scene: UIViewController)
}

extension ModuleRoutingProtocol {
    func pushToScene(_ scene: UIViewController) { }
    func presentScene(_ scene: UIViewController) { }
}

protocol ModuleRoutingBackProtocol: class {
    func calledBackFromRouter()
}

protocol ReusableView: class {}

extension ReusableView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UIViewController: ReusableView { }

extension UIStoryboard {
    
    func instantiateViewController<T>() -> T where T: ReusableView {
        return instantiateViewController(withIdentifier: T.reuseIdentifier) as! T
    }
}
