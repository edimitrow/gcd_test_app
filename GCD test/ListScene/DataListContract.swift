//
//  DataListContract.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 11/27/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

protocol DataListViewProtocol: class {
    
}

protocol DataListPresenterProtocol: class {
    
}

protocol DataListInteractorProtocol: class {
    
}

protocol DataListInteractorOutputProtocol: class {
    
}


