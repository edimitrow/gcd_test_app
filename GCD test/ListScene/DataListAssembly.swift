//
//  DataListAssembly.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 27/11/2018.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class DataListAssembly {

    weak var view: UIViewController?
    
    static func setupScene() -> DataListViewController {
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController() as DataListViewController
        
        let interactor = DataListInteractor()
        let presenter = DataListPresenter(interactor)
        viewController.presenter = presenter
        
        presenter.attachView(viewController)
        interactor.attachOutput(presenter)
        
        return viewController
    }
}
