//
//  DataListTableViewDataSource.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 27/11/2018.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

extension DataListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return tableView.dequeueReusableCell(withIdentifier: "DataCell", for: indexPath)
    }
}
