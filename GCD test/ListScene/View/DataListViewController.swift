//
//  DataListViewController.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 11/26/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class DataListViewController: UIViewController {
    
    @IBOutlet weak var dataTableView: UITableView!
    
    var presenter: DataListPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func loadRemoteDataAction(_ sender: Any) {
        
    }

}

extension DataListViewController: DataListViewProtocol {
    
}

extension DataListViewController: ModuleRoutingProtocol {
    
//    need to make custom animation for view appearance
    
//    func presentScene(_ scene: UIViewController) { }
//
//    func pushToScene(_ scene: UIViewController) {
//        self.navigationController?.pushViewController(scene, animated: true)
//    }
    
}
