//
//  DataListPresenter.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 11/27/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class DataListPresenter {

    weak var view: DataListViewProtocol?
    var interactor: DataListInteractorProtocol?
    
    func attachView(_ view: DataListViewProtocol) {
        self.view = view
    }
    
    init(_ interactor: DataListInteractorProtocol) {
        self.interactor = interactor
    }
}

extension DataListPresenter: DataListPresenterProtocol {

}

extension DataListPresenter: DataListInteractorOutputProtocol {

}
