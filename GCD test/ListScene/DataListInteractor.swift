//
//  DataListInteractor.swift
//  GCD test
//
//  Created by Eugene Dimitrow on 11/27/18.
//  Copyright © 2018 Eugene Dimitrow. All rights reserved.
//

import UIKit

class DataListInteractor {

    weak var output: DataListInteractorOutputProtocol?
    
    func attachOutput(_ output: DataListInteractorOutputProtocol?) {
        self.output = output
    }
}

extension DataListInteractor: DataListInteractorProtocol {
    
}
